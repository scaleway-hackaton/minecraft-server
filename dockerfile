FROM openjdk:11.0.10-jdk

WORKDIR /minecraft/
RUN wget https://papermc.io/api/v2/projects/paper/versions/1.16.5/builds/567/downloads/paper-1.16.5-567.jar
RUN mv paper-*.jar paper.jar

COPY eula.txt .
COPY server.properties .

RUN useradd -ms /bin/bash minecraft
RUN chown minecraft -R /minecraft

USER minecraft

CMD ["java", "-jar", "paper.jar"]